<?php

use Gezondtransport\Password\Legacy;

class PasswordLegacyTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->legacy = new Legacy;
    }

    public function testInstantiateAsObjectSucceeds()
    {
        $this->assertInstanceOf('Gezondtransport\Password\Legacy', $this->legacy);
    }

    public function testHashVerifySucceeds()
    {
        $password = 'Welkom@01';
        $wrongPassWord = 'WrongPass0wrd#';

        $hash = $this->legacy->hash($password);

        $verifySuccess = $this->legacy->verify($password, $hash);
        $verifyFailed   = $this->legacy->verify($wrongPassWord, $hash);

        $this->assertTrue($verifySuccess);
        $this->assertFalse($verifyFailed);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testHashInvalidArgumentException()
    {
        $this->legacy->hash();
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testVerifyInvalidArgumentException()
    {
        $this->legacy->verify();
    }
}
