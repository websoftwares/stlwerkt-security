<?php

use Gezondtransport\Security;

class SecurityTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->secretKey = 'j6644VBfKDua9ao86VW0t9y8285dlNo9';
    }

    public function testInstantiateAsObjectSucceeds()
    {
        $this->assertInstanceOf('Gezondtransport\Security', new Security);
    }

    public function testPasswordInstantiateObjectSucceeds()
    {
        // Interface name
        $expectedInterface = 'Gezondtransport\Password';
        $actual = Security::Password();
        $this->assertEquals($expectedInterface, key(class_implements($actual)));
    }

    public function testCsrfInstantiateObjectSucceeds()
    {
        // Interface name
        $expectedInterface = 'Gezondtransport\Csrf';
        $actual = Security::CSRF($this->secretKey);
        $this->assertEquals($expectedInterface, key(class_implements($actual)));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testCsrfInvalidArgumentException()
    {
         Security::CSRF();
    }
}
