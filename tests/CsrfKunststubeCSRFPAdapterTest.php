<?php

use Gezondtransport\Csrf\KunststubeCSRFPAdapter;

class CsrfKunststubeCSRFPAdapterTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->secretKey = 'j6644VBfKDua9ao86VW0t9y8285dlNo9';
        $this->csrf = new KunststubeCSRFPAdapter($this->secretKey);
    }

    public function testInstantiateAsObjectSucceeds()
    {
        $this->assertInstanceOf('Gezondtransport\Csrf\KunststubeCSRFPAdapter', $this->csrf);
    }

    public function testGenerateGetValidateSignature()
    {
        $token = $this->csrf->getSignature();
        $invalidToken = '1167839616:q8ri61sjLgg7BraVK3mY5b0N3fN9zFyCPVvSaCIg29in9fWmdtaq6K/TO8Q77za/MwFVBwji9nvcmBVxDOF+WA==:uFtMshL7h1zGXA2ractEYuKXjei45sEjg4pPuTrz8uP+P1zHYYo8e6ohLW/9hCePkw5yD7ZeMZbPqTHurBLh2Q==';
        $this->assertTrue($this->csrf->validateSignature($token));
        $this->assertFalse($this->csrf->validateSignature($invalidToken));
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testGenerateSignatureInvalidArgumentException()
    {
         $this->csrf->generateSignature();
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testValidateSignatureInvalidArgumentException()
    {
         $this->csrf->validateSignature();
    }
}
