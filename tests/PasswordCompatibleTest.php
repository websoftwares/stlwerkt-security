<?php

use Gezondtransport\Password\Compatible;

class PasswordCompatibleTest extends \PHPUnit_Framework_TestCase
{
    public function setUp()
    {
        $this->compatible = new Compatible;
    }

    public function testInstantiateAsObjectSucceeds()
    {
        $this->assertInstanceOf('Gezondtransport\Password\Compatible', $this->compatible);
    }

    public function testHashVerifySucceeds()
    {
        $password = 'Welkom@01';
        $wrongPassWord = 'WrongPass0wrd#';

        $hash = $this->compatible->hash($password);

        $verifySuccess = $this->compatible->verify($password, $hash);
        $verifyFailed   = $this->compatible->verify($wrongPassWord, $hash);

        $this->assertTrue($verifySuccess);
        $this->assertFalse($verifyFailed);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testHashInvalidArgumentException()
    {
        $this->compatible->hash();
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testVerifyInvalidArgumentException()
    {
        $this->compatible->verify();
    }
}
