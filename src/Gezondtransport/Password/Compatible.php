<?php
/**
* Gezondtransport\Password\Compatible
*
* Class voor het maken en valideren van password hashes
* Dit is voor het gebruik in systemen met PHP 5.3.7 t/m 5.5.0
*
* @package Gezondtransport
* @subpackage Password
* @author boris <boris@gezondtransport.nl>
*/
namespace Gezondtransport\Password;

class Compatible implements \Gezondtransport\Password
{
    /**
     * hash
     * hashes the password by algorithm and options
     *
     * @param string $password
     * @param string $algorithm
     * @param array  $options
     *
     * @return string
     */
    public function hash($password = null, $algorithm = null, array $options = array())
    {
        if (! $password) {
            throw new \invalidArgumentException('password is a required argument');
        }

        // Make sure we set it to blowfish if no arg is set
        if (! $algorithm) {
            // Blowfish
            $algorithm = PASSWORD_BCRYPT;
        }

        return \password_hash($password, $algorithm, $options);
    }

    /**
     * verify
     * verify password by hash
     *
     * @param string $password
     * @param string $hash
     *
     * @return boolean
     */
    public function verify($password = null, $hash = null)
    {
        if (! $password && ! $hash) {
            throw new \invalidArgumentException('password and hash are required arguments');
        }

        return \password_verify($password, $hash);
    }
}
