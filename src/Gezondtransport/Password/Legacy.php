<?php
/**
* Gezondtransport\Password\Legacy
*
* Class voor het maken en valideren van password hashes
* Dit is voor het gebruik in systemen met PHP 5.3.2 t/m 5.2.6
*
* @package Gezondtransport
* @subpackage Password
* @author boris <boris@gezondtransport.nl>
*/
namespace Gezondtransport\Password;

use PasswordLib\PasswordLib;

class Legacy implements \Gezondtransport\Password
{

    /**
     * $lib
     * @var object
     */
    public $lib = null;

    public function __construct()
    {
        $this->lib = new PasswordLib;
    }

    /**
     * hash
     * hashes the password by algorithm and options
     *
     * @param string $password
     * @param string $algorithm
     * @param array  $options
     *
     * @return string
     */
    public function hash($password = null, $algorithm = null, array $options = array())
    {
        if (! $password) {
            throw new \invalidArgumentException('password is a required argument');
        }

        // Make sure we set it to blowfish if no arg is set
        if (! $algorithm) {
            // Blowfish
            $algorithm = '$2a$';
        }

        return $this->lib->createPasswordHash($password, $algorithm, $options);
    }

    /**
     * verify
     * verify password by hash
     *
     * @param string $password
     * @param string $hash
     *
     * @return boolean
     */
    public function verify($password = null, $hash = null)
    {
        if (! $password && ! $hash) {
            throw new \invalidArgumentException('password and hash are required arguments');
        }

        return $this->lib->verifyPasswordHash($password, $hash);
    }
}
