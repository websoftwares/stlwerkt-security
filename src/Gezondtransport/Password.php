<?php
/**
* Password interface
* Dicteerd dat de methodes in dit bestand worden gebruikt voor password classes.
*
* @author Boris <boris@gezondtransport.nl
* @package Gezondtransport
*/
namespace Gezondtransport;

interface Password
{
    /**
     * hash
     * hashes the password by algorithm and options
     *
     * @param string $password
     * @param string $algorithm
     * @param array  $options
     *
     * @return string
     */
    public function hash($password = null, $algorithm = null, array $options = array());

    /**
     * verify
     * verify password by hash
     *
     * @param string $password
     * @param string $hash
     *
     * @return boolean
     */
    public function verify($password = null, $hash = null);
}
