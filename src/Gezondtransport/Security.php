<?php
/**
 * Security
 * Class voor het afhandelen van beveiligingen
 *
 * @package Gezondtransport
 * @author Boris <boris@gezondtransport.nl
 */
namespace Gezondtransport;

class Security
{
    /**
     * Password
     * Factory Method for factoring a password class.
     * Depending on php version.
     *
     * @return object
     */
    public static function Password()
    {

        if (version_compare(PHP_VERSION, '5.5.0') >= 0) {
            $password = 'Native';
        }

        if (version_compare(PHP_VERSION, '5.3.7') >= 0) {
            $password = 'Compatible';
        }

        if (version_compare(PHP_VERSION, '5.3.2') >= 0) {
            $password = 'Legacy';
        }

        $password =  __NAMESPACE__ . '\\Password\\' . $password;
        if (class_exists($password)) {
            return new $password;

        } else {
            throw new \OutOfRangeException($password . 'Not found');
        }
    }

    /**
     * CSRF
     * Factory Method for factoring a CSRF class.
     *
     * @param string $secretKey een goede geheime sleutel
     * @param string $adapter Adapter class for csrf library of choice as long it implements the Csrf interface
     * @return object
     */
    public static function CSRF($secretKey = null, $adapter = 'KunststubeCSRFPAdapter')
    {
        if(! $secretKey) {
            throw new \InvalidArgumentException($secretKey . 'secretKey cannot be empty');
        }

        $adapter = __NAMESPACE__ . '\\Csrf\\' . $adapter;
        return new $adapter($secretKey);
    }
}
