<?php
/**
* Csrf interface
* Dicteerd dat de methodes in dit bestand worden gebruikt voor csrf classes.
*
* @author Boris <boris@gezondtransport.nl
* @package Gezondtransport
*/
namespace Gezondtransport;

interface Csrf
{
    /**
     * generateSignature
     *
     * @param  string $secretKey
     */
    public function generateSignature($secretKey = null);

    /**
     * getSignature
     * @return string
     */
    public function getSignature();

    /**
     * validateSignature
     * @param  string $token
     */
    public function validateSignature($token = null);
}