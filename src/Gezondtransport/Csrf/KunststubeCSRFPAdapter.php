<?php
/**
* Gezondtransport\Csrf\KunststubeCSRFPAdapter
*
* Class adapter voor \Kunststube\CSRFP package
*
* @package Gezondtransport
* @subpackage Csrf
* @author boris <boris@gezondtransport.nl>
*/
namespace Gezondtransport\Csrf;

class KunststubeCSRFPAdapter implements \Gezondtransport\Csrf
{
    /**
     * $csrf
     * @var object
     */
    protected $csrf = null;

    /**
     * __construct
     * @param string $secretKey
     */
    public function __construct($secretKey = null)
    {
        if($secretKey) {
            $this->generateSignature($secretKey);
        } else {
            throw new \invalidArgumentException($secretKey . 'cannot be null');
        }
    }

    /**
     * generateSignature
     *
     * @param  string $secretKey
     * @return object
     */
    public function generateSignature($secretKey = null)
    {
        if (!$secretKey) {
            throw new \invalidArgumentException($secretKey . 'cannot be null');
        }

        $this->csrf = new \Kunststube\CSRFP\SignatureGenerator($secretKey);
        return $this;
    }

    /**
     * getSignature
     * @return string
     */
    public function getSignature()
    {
        return $this->csrf->getSignature();
    }

    /**
     * validateSignature
     * @param  string $token
     */
    public function validateSignature($token = null)
    {
        if (!$token) {
            throw new \invalidArgumentException($token . 'cannot be null');
        }
        return $this->csrf->validateSignature($token);
    }
}
