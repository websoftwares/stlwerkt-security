# Gezondtransport\Security



Deze repository is voor alle beveiliging gerelateerde packages en subpackages.

## Installeren

Gebruik composer om de dependencies te installeren

```
php composer.phar install
```

### Password

De password libraries zorgen ervoor dat het password gehashed wordt
met de meest sterk mogelijke encryptie methode afhankelijk van de php versie

*    Legacy (PHP_VERSION 5.3.2 t/m 5.3.6)
*    Compatible (PHP_VERSION 5.3.7 t/m 5.4.*)
*    Native (PHP_VERSION 5.5.0)

```php
use Gezondtransport\Security;

$hash = Security::Password()->hash('Welkom01')

if(Security::Password()->verify('Welkom01', $hash)) {
    // Good
} else {
    // Error
}

```

### CSRF

De CSRF Libraries zorgen voor bescherming tegen Cross-site request forgery aanvallen.

```php
use Gezondtransport\Security;

$secretKey = 'j6644VBfKDua9ao86VW0t9y8285dlNo9';

$token = Security::CSRF($secretKey)->getSignature();

// Later

if(Security::CSRF($secretKey)->validateSignature($token)) {
    // Good
} else {
    // Error
}

```
